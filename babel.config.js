module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    ["module-resolver", {
      "root": ["./src"],
      "alias": {
        "@app": "./src/app",
        "@assets": "./src/assets",
        "@components": "./src/components",
        "@config": "./src/config",
        "@locales": "./src/locales",
        "@navigation": "./src/navigation",
        "@pages": "./src/pages",
        "@services": "./src/services",
        "@src": "./src",
        "@store": "./src/store",
        "@themes": "./src/themes",
        "@utils": "./src/utils"
      }
    }]
  ]
};

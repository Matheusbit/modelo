import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import getTheme from "./native-base-theme/components";
import material from "./native-base-theme/variables/material";
import { createStore, applyMiddleware } from "redux";
import { StyleProvider, Root } from "native-base";
import { Provider } from "react-redux";
import createSagaMiddleware from "redux-saga";
import ReduxNavigation from "@navigation/redux.js";
import middlewares from "@store/middleware";
import rootSaga from "@store/middleware/sagas";
import reducers from '@store/reducers';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  reducers,
  {},
  applyMiddleware(sagaMiddleware, ...middlewares)
);

sagaMiddleware.run(rootSaga);

export default class App extends Component {
  render() {
    return (
      <Root>
        <StyleProvider style={getTheme(material)}>
          <Provider store={store}>
            <ReduxNavigation/>
          </Provider>
        </StyleProvider>
      </Root>
    );
  }
}
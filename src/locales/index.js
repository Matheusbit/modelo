import React from 'react';
import i18n from 'i18n-js';
import * as RNLocalize from "react-native-localize";

let languageCode = RNLocalize.getLocales()[0].languageCode;

i18n.fallbacks = true;
i18n.translations = {
    pt: require('./languages/pt.json'),
    en: require('./languages/en.json')
};
i18n.locale = languageCode;

RNLocalize.addEventListener("change", () => {
    i18n.locale = RNLocalize.getLocales()[0].languageCode;
});

export default i18n; 
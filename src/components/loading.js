import React from "react";
import { compose, withProps, lifecycle } from "recompose";
import { Container, Content, List, ListItem, Text } from "native-base";
import { logout } from "@services/auth";
import { ActivityIndicator, View } from "react-native";
import { colors } from "@themes";
import { isAuthenticated } from '@services/auth';

const LoadingComponent = ({ props }) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.white,
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <ActivityIndicator size="large" color={colors.primary} />
    </View>
  );
};

const enhance = compose(
  lifecycle({
    async componentDidMount() {
        const authenticated = await isAuthenticated();
        authenticated ?
            this.props.navigation.navigate('homePage')
            :
            this.props.navigation.navigate('loginPage')
    }
  }),
  withProps(props => {
    return {
      props: props
    };
  })
);

const enhanced = enhance(LoadingComponent);

export default enhanced;

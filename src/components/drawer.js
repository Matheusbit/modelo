import React from "react";
import { compose, withProps, lifecycle } from "recompose";
import { Container, Content, List, ListItem, Text } from "native-base";
import { logout } from '@services/auth';

const DrawerComponent = ({ props }) => {
  return (
    <Container>
      <Content> 
        <ListItem onPress={() => null}>
          <Text>Meu perfil</Text>
        </ListItem>
        <ListItem onPress={() => null}>
          <Text>Sobre</Text>
        </ListItem>
        <ListItem onPress={() => null}>
          <Text>Contato</Text>
        </ListItem>
        <ListItem itemDivider>
        </ListItem>
        <ListItem onPress={() => { logout(); props.navigation.navigate('loginPage')}}> 
          <Text>Sair</Text>
        </ListItem>
      </Content>
    </Container>
  );
};

const enhance = compose(
  lifecycle({
    componentDidMount() {}
  }),
  withProps(props => {
    return {
      props: props
    };
  })
);

const enhanced = enhance(DrawerComponent);

export default enhanced;

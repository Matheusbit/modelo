import React from "react";
import {
  compose,
  withProps,
  lifecycle
} from "recompose";
import {
  Header,
  Left,
  Right,
  Body,
  Title
} from "native-base";

const HeaderComponent = ({ props }) => {
  return (
    <Header iosBarStyle={"light-content"}>
      <Left style={{paddingLeft: 10}}>{props.left && props.left()}</Left>
      <Body>
        <Title style={{fontWeight: '500'}}>{props.title}</Title>
      </Body>
      <Right>{props.right && props.right()}</Right>
    </Header>
  );
};

const enhance = compose(
  lifecycle({
    componentDidMount() {}
  }),
  withProps(props => {
    return {
      props: props
    };
  })
);

const enhanced = enhance(HeaderComponent);

export default enhanced;

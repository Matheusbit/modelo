import React from 'react';
import { compose, withProps, lifecycle } from 'recompose';
import { Button, Text } from 'native-base';

const ButtonComponent = (
  { 
    props
  }
) => {
  return (
    <Button 
        block 
        onPress={() => props.onPress()}
        style={props.style}
        disabled={props.disabled}
    >
        <Text>{props.title}</Text>
    </Button>
  )
};

const enhance = compose(
  lifecycle({
    componentDidMount() {

    }
  }),
  withProps((props) => {
    return {
      props: props
    }
  })
)

const enhanced = enhance(ButtonComponent);

export default enhanced;
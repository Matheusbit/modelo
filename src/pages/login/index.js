import React, { Component } from "react";
import { Image } from "react-native";
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from "recompose";
import { connect } from "react-redux";
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Toast,
  Label,
  Button,
  Text,
  Row
} from "native-base";
import Logo from "@assets/logo.svg";
import { moderateScale } from "@utils/layout.js";
import style from "./style.js";
import ButtonComponent from "@components/button";
import i18n from "@locales";
import { Types, Creators } from "@store/reducers/loginReducer";

const navigationOptions = ({ navigation }) => {
  return {
    header: null
  };
};

const LoginPage = ({ props, username, password }) => {
  return (
    <Container>
      <Content contentContainerStyle={style.content}>
        <Image
          source={require("@assets/logo.png")}
          style={{ width: moderateScale(140), height: moderateScale(140) }}
          resizeMode={"contain"}
        />
        <Form>
          <Item stackedLabel>
            <Label style={style.label}>{i18n.t("login.login")}</Label>
            <Input
              style={style.input}
              onChangeText={text => props.setusername(text)}
              value={username}
            />
          </Item>
          <Item stackedLabel>
            <Label style={style.label}>{i18n.t("login.senha")}</Label>
            <Input
              style={style.input}
              secureTextEntry={true}
              onChangeText={text => props.setpassword(text)}
              value={password}
            />
          </Item>
        </Form>
        <ButtonComponent
          title={!props.loading ? i18n.t("login.login") : i18n.t("form.loading")}
          onPress={() => props.login({username, password})}
          style={style.button}
          disabled={props.loading}
        />
      </Content>
    </Container>
  );
};
 
const enhance = compose(
  withState("username", "setusername",  "user"), 
  withState("password", "setpassword", "12345678"),
  withProps(props => {
    return {
      props: props
    };
  })
);

const mapStateToProps = state => ({
  loading: state.loginReducer.loading,
});

const mapDispatchToProps = dispatch => {
  return {
    login: form => dispatch(Creators.login_request(form))
  };
};

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(enhance(LoginPage));

connected.navigationOptions = navigationOptions;

export default connected;

import { StyleSheet } from 'react-native';
import { colors } from '@themes';

export default StyleSheet.create({
  button: {
    marginTop: 20,
    marginHorizontal: 10,
    borderRadius: 5
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center', 
    width: '100%',
    backgroundColor: colors.white
  },
  label: { 
    color: colors.text
  },
  input: {
    color: colors.input
  }
});

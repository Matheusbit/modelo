import { StyleSheet } from "react-native";
import { colors, metrics } from "@themes";
import { moderateScale, verticalScale, width } from "@utils/layout.js";

export default StyleSheet.create({
  button: {
    marginTop: 20,
    marginHorizontal: 10,
    borderRadius: 5
  },
  colButton: {
    height: width / 2,
    justifyContent: "space-around",
    alignItems: "center"
  },
  content: {
    backgroundColor: colors.secondary
  },
  gridButtons: {
    borderWidth: metrics.borderWidth,
    borderColor: colors.borderColor
  },
  icon: {
    fontSize: moderateScale(60),
    color: colors.primary
  },
  iconMenu: {
    fontSize: moderateScale(30),
    color: colors.white
  },
  input: {
    color: colors.input
  },
  label: {
    color: colors.text,
    fontSize: moderateScale(20),
    fontWeight: "500"
  },
  subTitle: {
    color: colors.text,
    fontSize: moderateScale(17),
    fontWeight: "500"
  }
});

import React, { Component } from "react";
import { TouchableOpacity, ScrollView } from "react-native";
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from "recompose";
import { connect } from "react-redux";
import { Container, Content, Col, Row, Grid, Text } from "native-base";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Logo from "@assets/logo.svg";
import { moderateScale, verticalScale, width } from "@utils/layout.js";
import style from "./style.js";
import ButtonComponent from "@components/button";
import HeaderComponent from "@components/header";
import { colors, metrics } from '@themes';
import i18n from "@locales";
import { Creators } from "@store/reducers/loginReducer";

const navigationOptions = ({ navigation }) => {
  return {
    header: 
    <HeaderComponent 
      navigation={navigation}
      title={i18n.t("home.title")} 
      left={() => 
        <FontAwesome5 
          name="bars" 
          style={style.iconMenu} 
          onPress={() => {
            navigation.toggleDrawer();
          }}
        />
      }
    />
  };
};

const HomePage = ({ props }) => {
  return (
    <Container>
      <ScrollView>
        <Row style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginVertical: verticalScale(12)
        }}>
          <Text style={style.subTitle}>{i18n.t("home.subtitle")}</Text>  
        </Row> 
        <Grid style={style.gridButtons}>
          <Row>  
            <Col style={[{ borderRightWidth: metrics.borderWidth,  borderBottomWidth: metrics.borderWidth, borderColor: colors.borderColor }, style.colButton ]} >
              <TouchableOpacity style={[{flex: 1 }, style.colButton]}>
                <FontAwesome5 name="file-invoice" style={style.icon}/>
                <Text style={style.label}>Invoice Register</Text>
              </TouchableOpacity>
            </Col>
            <Col style={[{ borderBottomWidth: metrics.borderWidth, borderColor: colors.borderColor}, style.colButton ]} >
              <TouchableOpacity style={[{flex: 1 }, style.colButton]}>
                <FontAwesome5 name='clipboard-list' style={style.icon}/>
                <Text style={style.label}>Stock</Text> 
              </TouchableOpacity>
            </Col>
          </Row>
          <Row>
            <Col style={[{ borderRightWidth: metrics.borderWidth, borderColor: colors.borderColor}, style.colButton]} >
              <TouchableOpacity style={[{flex: 1 }, style.colButton]}>
                <FontAwesome5 name="tags" style={style.icon}/>
                <Text style={style.label}>Equipment Bond</Text>
              </TouchableOpacity>
            </Col>
            <Col style={style.colButton} >
              <TouchableOpacity style={[{flex: 1 }, style.colButton]}>
                <FontAwesome5 name="search" style={style.icon}/>
                <Text style={style.label}>Search</Text>
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>
      </ScrollView>
    </Container>
  );
};

const enhance = compose(
  withProps(props => {
    return {
      props: props
    };
  })
);

const mapStateToProps = state => ({
  // id: state.usuarioReducer.id,
});

const mapDispatchToProps = dispatch => {
  return {
    login: form => dispatch(Creators.login_request(form))
  };
};

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(enhance(HomePage));

connected.navigationOptions = navigationOptions;

export default connected;

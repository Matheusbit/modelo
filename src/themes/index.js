export const colors = {
    primary: '#226ABD',
    secondary: '#373435',
    white: '#fff',
    input: '#373435',
    text: '#373435',
    borderColor: '#373435'
}

export const metrics = {
    borderWidth: 0.6
}
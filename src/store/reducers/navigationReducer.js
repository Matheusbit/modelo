import AppStack from "@navigation/routes.js";

const router = AppStack.router;
const homeNavAction = router.getActionForPathAndParams('loginPage');
const initialNavState = router.getStateForAction(homeNavAction);

export const nav = (state, action) => {
  const newState = router.getStateForAction(action, state);  
  return newState || state;
};

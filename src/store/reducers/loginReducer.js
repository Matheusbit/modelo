import { createReducer, createActions } from 'reduxsauce'

export const INITIAL_STATE  = {
    "id": null,
    "loading": false,
    "error": null
};

export const login_request = (state = INITIAL_STATE, action) => {
    return {...state, loading: true}
}

export const login_success = (state = INITIAL_STATE, action) => {
    return {...state, loading: false}
}

export const login_failed = (state, action) => {
    return {...state, loading: false, error: action.error}
}
3
export const { Types, Creators } = createActions({
    login_success: null,
    login_failed: ['error'],
    login_request: ['form'],
})

export default loginReducer = createReducer(INITIAL_STATE, {
    [Types.LOGIN_SUCCESS]: login_success,
    [Types.LOGIN_FAILED]: login_failed,
    [Types.LOGIN_REQUEST]: login_request,
})
  
import { combineReducers } from 'redux';
import { resettableReducer } from 'reduxsauce'
import loginReducer from './loginReducer.js';
import { nav } from './navigationReducer.js';

export const RESET_LOGIN = "RESET_LOGIN"
const resetTableLogin = resettableReducer(RESET_LOGIN)

export default combineReducers({
    nav: nav,
    loginReducer: resetTableLogin(loginReducer),
}); 
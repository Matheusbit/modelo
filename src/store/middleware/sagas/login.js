import { takeLatest, put } from "redux-saga/effects";
import api from "@services/api";
import { login } from "@services/auth";
import { Types, Creators } from "@store/reducers/loginReducer";
import decode from "jwt-decode";

function* auth(action) {
  try {
    const response = yield api.post(`login`, action.form);
    const data = yield response.data;
    login(data.token);
    yield put({ type: "Navigation/NAVIGATE", routeName: "homePage" });
    yield put(Creators.login_success());
  } catch (e) {
    if (e.response && e.response.data && e.response.data.message) {
      alert(e.response.data.message);
      yield put(Creators.login_failed(e.response.data.message));
    } else { 
      alert(e.message);
      yield put(Creators.login_failed("Erro ao autenticar"));
    }
  }
}

export function* loginSaga() {
  yield takeLatest(Types.LOGIN_REQUEST, auth);
}

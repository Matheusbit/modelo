import { put, takeEvery, all } from 'redux-saga/effects';
import { helloSaga } from "./hello";
import { loginSaga } from './login';

export default function* rootSaga() {
    yield all([
      helloSaga(),
      loginSaga(),
    ])
}
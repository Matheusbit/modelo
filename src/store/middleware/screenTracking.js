import { NavigationActions } from "react-navigation";
import { isAuthenticated } from "@services/auth";

const screenTracking = ({ getState }) => {
  return next => action => {
    return isAuthenticated().then(data => {
      return next(action);
    })
    .catch( error => {
      return next({type: "Navigation/NAVIGATE", routeName: "homePage"});   
    });
  };
};

export default screenTracking;
import screenTracking from './screenTracking';
import createSagaMiddleware from 'redux-saga'
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';

const middleware = createReactNavigationReduxMiddleware(
    state => state.nav,
    'root'
  );
 
export default middlewares = [middleware];
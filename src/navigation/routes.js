import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import { createAppContainer } from "react-navigation";
import LoginPage from "@pages/login";
import HomePage from "@pages/home";
import { useScreens } from "react-native-screens";
useScreens();
import DrawerComponent from "@components/drawer";
import LoadingComponent from '@components/loading';

const Stack = createStackNavigator(
  {
    homePage: {
      screen: HomePage
    }
  },
  {
    initialRouteName: "homePage"
  }
);

const DrawerStack = createDrawerNavigator(
  {
    Stack: {
      screen: Stack
    }
  },
  {
    contentComponent: props => <DrawerComponent {...props} />
  }
);

const AppStack = createStackNavigator(
  {
    nada: {
      screen: LoadingComponent
    },
    loginPage: {
      screen: LoginPage
    },
    DrawerStack: {
      screen: DrawerStack
    }
  },
  {
    headerMode: "none",
    navigationOptions: {
      headerVisible: false
    }
  }
);

export default createAppContainer(AppStack);

import * as React from "react";
import { BackHandler, Platform } from "react-native";
import {
  createReactNavigationReduxMiddleware,
  createReduxContainer
} from "react-navigation-redux-helpers";
import { connect } from "react-redux";
import AppStack from "./routes.js";

const ReduxAppNavigator = createReduxContainer(AppStack, "root");

class ReduxNavigation extends React.Component {
  async componentDidMount() {
    if (Platform.OS === "ios") return;
    BackHandler.addEventListener("hardwareBackPress", () => {
      const { dispatch, nav } = this.props;
      return false;
    });
  }
 
  componentWillUnmount() {
    if (Platform.OS === "ios") return;
    BackHandler.removeEventListener("hardwareBackPress", undefined);
  }

  render() {
    return (
      <ReduxAppNavigator
        dispatch={this.props.dispatch}
        state={this.props.nav}      
      />
    );
  }
}

const mapStateToProps = state => ({
  nav: state.nav
});
export default connect(mapStateToProps)(ReduxNavigation);

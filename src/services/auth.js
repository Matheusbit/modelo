import decode from "jwt-decode";
import AsyncStorage from "@react-native-community/async-storage";

export const TOKEN_KEY = "xxxx-token";

export const isAuthenticated = () => {
  return getToken()
    .then(token => {
      console.log(token)
      return !isTokenExpired(token);
    })
    .catch(error => {
      return false;
    });
};

export const getToken = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const value = await AsyncStorage.getItem("@token");
      if (value !== null) {
        resolve(value);
      } else {
        reject();
      }
    } catch (e) {
      reject();
    }
  });
};

export const login = async token => {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.setItem("@token", token);
      resolve();
    } catch (e) {
      reject();
    }
  });
};

export const logout = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.clear();
      resolve();
    } catch (e) {
      reject();
    }
  });
};

export const isTokenExpired = token => {
  try {
    const decoded = decode(token);
    if (decoded.exp < Date.now() / 1000) {
      return true;
    } else return false;
  } catch (err) {
    return false;
  }
};

export const logedUser = () => {
  try {
    const decoded = decode(getToken());
    return decoded.sub;
  } catch (err) {
    return false;
  }
};

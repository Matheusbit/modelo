import axios from "axios";
import { getToken } from "./auth";

const api = axios.create({
  baseURL: "xxxxxxx",
  responseType: "json"
});

export const encode = data => {
  var formBody = [];
  for (var property in data) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(data[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");
  return formBody;
};
 
export default api; 